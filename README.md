# GDAL #

## One step package creation

        conan create . terranum-conan+gdal/stable -o gdal:with_curl=True -o gdal:shared=True

Building under OSX (version 3.7.0) worked with the following command:

        conan create . terranum-conan+gdal/stable -o gdal:with_curl=True -o gdal:shared=True --build=gdal --build=libgeotiff        


## Upload package to gitlab

      conan upload gdal/3.7.0@terranum-conan+gdal/stable -q --remote=gitlab --all

or

      conan upload gdal/3.7.0@terranum-conan+gdal/stable -q 'build_type=Release'  --remote=gitlab


## Build the debug package

      conan create . terranum-conan+gdal/stable -s build_type=Debug

Don't upload debug package on Gitlab.

## Step by step package creation

1. Get source code

        conan source . --source-folder=_bin/source
2. Create install files

        conan install . --install-folder=_bin/build -o with_curl=True
3. Build

        conan build . --source-folder=_bin/source --build-folder=_bin/build
4. Create package 

        conan package . --source-folder=_bin/source --build-folder=_bin/build --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+gdal/stable --source-folder=_bin/source --build-folder=_bin/build
6. Test package
      
        conan test test_package gdal/3.7.0@terranum-conan+gdal/stable

## Linux problems

The version of cmake distributed by Ubuntu 20 is too old (3.16) while the current version of cmake is 3.24. This causes problems with GDAL 3.5.2. 
To solve this problem, you have to install the latest version of cmake via the apt repository of kitware (see here for more information https://apt.kitware.com/)

